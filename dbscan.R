construct_labDBSCAN <- function(assignment, sizes, data){
  res <- list(
      cluster = assignment,
      sizes = sizes,
      data = data
    )
  class(res) <- append(class(res),"labDBSCAN")
  return(res)
}

#Finde Nachbarn des gegebenen Objekts
getNeighbors <- function(data, eps){
  
  #Gebe die Indexe der Objekte zurueck, die von dem gegebenen Objekt erreichbar sind
  return(which(as.vector(data) <= eps))
}


dbscan <- function(data, MinPts = 3, eps = 1, dist.fun = "euclidean", auto.params = F){
  #Parametertest von dist.fun
  if(class(dist.fun) == "character"){
    #Ueberpruefung des uebergebenen Strings
    
    #Erstelle Distanzmatrix mit Hilfe der euklidischen Distanz
    if(dist.fun == "euclidean"){
      dists <- dist(data, method = "euclidean", diag = TRUE, upper = TRUE)
      distMat <- as.matrix(dists)
      
    #Erstelle Distanzmatrix mit Hilfe der Maximums Distanz
    } else if(dist.fun == "maximum"){
      dists <- dist(data, method = "maximum", diag = TRUE, upper = TRUE)
      distMat <- as.matrix(dists)
      
    #Erstelle Distanzmatrix mit Hilfe der Manhattan Distanz
    } else if(dist.fun == "manhattan"){
      dists <- dist(data, method = "manhattan", diag = TRUE, upper = TRUE)
      distMat <- as.matrix(dists)
      
    #Fehler, da ungueltiger String Uebergeben wurde
    } else{
      stop("dist.fun: Keine gueltige Eingabe")
      
    } 
  } else{
    #Erstelle Distanzmatrix mit Uebergebener Distanzfunktion
    distMat <- dist.fun(data)
  }
  
  #Ueberpruefe, ob Anwender automatische Berechnung der Parameter anfordert
  if(auto.params == TRUE){
    
    #Berechne MinPts zur Dichteklassifizierung (2 mal die Dimension der Daten minus 1)
    minPTS <- 2*ncol(data)-1
    
    #Berechne die zu Ueberpruefende Distanz Epsilon
    
    #Finde fuer jeden Punkt i die kleinste Distanz, sodass i dicht ist
    #Sortiere die Distanzmatrix spaltenweise aufsteigend
    sortedDistMat <- apply(distMat, 2, sort)
    
    #Lese den Distanzvektor aus, fuer den jedes Element dicht ist
    minPTSDists <- as.vector(sortedDistMat[minPTS,])
    
    #Sortiere den Vector absteigend nach Distanzen
    minPTSDists <- sort(minPTSDists, decreasing = TRUE)
    
    #Berechne das "kink" fuer alle x_i in minPTSDists mit 1<i<length(minPTSDists)
    #y_i := minPTSDists[i]
    #kink(minPTSDists[i]) = |(y_i-y_(i+1))-(y_(i-1)-y_i)|
    #Zur einfacheren Berechnung iteriere ich über den ganzen Vektor und setze 
    #die Randstellen auf -Inf, da nach dem maximum gesucht wird
    kink <- unlist(sapply(seq_along(minPTSDists), function(i) if(1 < i && i < length(minPTSDists)){abs((minPTSDists[i]-minPTSDists[i+1])-(minPTSDists[i-1]-minPTSDists[i]))} else {-Inf}))
    
    #Suche die Distanz mit dem groessten kink-Wert und benutze diese als Epsilon
    epsilon <- minPTSDists[which.max(kink)]
    
  #Wenn Anwender die Parameter selber übergibt:
  } else{
    minPTS <- MinPts
    epsilon <- eps
  }
  
  #Vorab Deklaration des Vektors assignment mit Laenge ist Anzahl Zeilen(Daten) von data ("Laufzeit optimierung")
  assignment <- rep(0, times = nrow(data))
  
  #Vorab Deklaration einer Variable zur Besucht-Zertifizierung
  visited <- rep(0, times = nrow(data))
  
  #Ordne jedes Element seinem Cluster zu
  for(i in 1:ncol(distMat)){
    #Wenn Objekt i noch nicht klassifiziert ist, Uebergebe Objekt i an den Algorythmus
    if(assignment[i] == 0){
      
      #Erhalte die Indexe der Nachbarn von Objekt i
      neighbors <- getNeighbors(distMat[i,], epsilon)
      
      #Markiere Objekt i als besucht
      visited[i] <- 1
      
      #Wenn Anzahl der Nachbarn von i >= minPTS, dann setze i als Kernobjekt
      if(length(neighbors) >= minPTS){
        
        #Setze Clusternummer auf die momentan maximale Clusternummer plus 1
        cluster = (max(assignment) + 1)
        assignment[i] = cluster 
        
        #Suche solange die Nachbarn aller Nachbarn, bis sich nichts mehr aendert
        check <- T
        
        while(check){
          #Speichere die Nachbarn zwischen
          oldNeighbors <- neighbors
          
          #Nehme von allen Nachbarn die Nachbarn
          tempNeighbors <- mapply(function(x) if(!(visited[x])){getNeighbors(distMat[x,], epsilon)}, neighbors)
          
          #Entfernen der Listenstruktur aus tempNeighbors
          tempNeighbors <- unlist(tempNeighbors)
          
          #Hinzufuegen der neuen Nachbarn
          neighbors <- append(neighbors,tempNeighbors)
          
          #Filtern der Nachbarn, sodass jeder Wert nur noch einmal im Vektor enthalten ist
          neighbors <- unique(neighbors)
          
          #Markiere die alten Nachbarn als besucht
          visited[oldNeighbors] <- 1
          
          #Wenn keine neuen Nachbarn hinzugekommen sind beende die Suche
          if(length(oldNeighbors) == length(neighbors)){
            assignment[neighbors] <- cluster
            check <- F
          }
        }
      }
    }
  }
  
  #Klassifiziere nun jedes clusterlose Element (assignment[i] == 0) als Noise (assignment[i] = -1)
  #Aenderung: Noise bleibt 0
  assignment <- replace(assignment, assignment == 0, 0)
  
  #Berechne sizes
  
  #Lege einen Vektor an mit einer Länge die der Anzahl der Cluster plus 1 entspricht
  
  
  #Zaehle Anzahl der Noise-Objekte
  noises <- sum(assignment == -1)
  
  #Zaehle Anzahl der Elemente pro Cluster und schreibe sie in sizes
  sizes <- rep(0, times = 1)
  if(max(assignment) >= 1){
    sizes <- rep(0, times = (max(assignment) + 1))
    sizes[1:max(assignment)] <- mapply(function(x) sum(assignment == x), 1:max(assignment))
  }
  
  #Schreibe Anzahl der Noises an letzte Stelle von sizes
  sizes[length(sizes)] <- noises
  
  #Gebe DBSCAN Klasse zurueck
  return(construct_labDBSCAN(assignment, sizes, data))
}


plot.labDBSCAN<- function(lkd, ...) {
  # 1. 2D-Plot
  if (dim(lkd[[3]])[2] == 2) {
    # Data plotten
    plot(lkd[[3]], col = mapply(function(i) if(i == -1) {1}else {i+1}, lkd[[1]]))
  } 
  else {
    if (dim(lkd[[3]])[2] == 3){
      # 2. 3D-Plot
      library(scatterplot3d)
      plot3d <- scatterplot3d(lkd[[3]], main = "3D Plot", color = lkd[[1]])
    }
    else {
      if (dim(lkd[[3]])[2] > 3) {
        # 3. Hoeherdimensional (2D-Projektion-Plot)  
        data2d <- cmdscale(d = dist(lkd[[3]]), k = 2)
        plot(data2d, col = lkd[[1]])
      }
    }
  } 
}

