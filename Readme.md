rtree-dbscan
============

Benchmarking project to test the performance of using an [R-Tree indexing structure](https://github.com/davidmoten/rtree) vs. relying on established algorithms. The results do speak for themselves.


Setup
-----

Install the packages `microbenchmark`, `fpc` and [`rtree-r`](https://bitbucket.org/kosimo/rtree-r) (this means, you'll a working installation of need `rJava`, too).


Running
-------

The benchmarking file (`benchmark.R`) relies on already present data. Just have a look into it and hack it your way. It exposes the following method:

```
benchmark(data, n = 5000, eps = 0.001, times = 5)
```

`data` is, because of relying on R-trees, expected to be two-dimensional. `n` sets the amount of rows, `eps` is the epsilon parameter passed to the DBSCAN algorithms. `microbenchmark` is told to run each algorithm `times` times.

The same holds for the `equivalence` function, included in `equivalence.R`:

```
testEquivalence(data, n = 10000, eps = 0.001)
```


Results
-------

On a mediocre laptop with an Intel i5 CPU, the following results have been achieved:
![Benchmark results](benchmark.png)